from main import signup, login, path_profile, delete_profile, get_users
from pytest import fixture
import os, csv

@fixture
def create_and_delete_db():
    fieldnames = ["name", "email", "password", "age"]   
    filename = "database.csv"
    if os.path.exists(filename):
        os.remove(filename)
    with open(filename, 'w') as file:
        writer = csv.DictWriter(file, fieldnames=fieldnames)
        writer.writeheader()
    yield('')
    os.remove(filename)

def test_signup(create_and_delete_db):
    #Sign-up user
    given = {   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": '21'
    }
    signed_up_user = signup(given)
    
    expected = {
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": '21'
    }
    assert signed_up_user == expected    

def test_main(create_and_delete_db):
    
    #Sign-up user
    given = {   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": '21'
    }
    signup(given)
    
    #Do login
    given = {   
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123"
    }   
    user_logged = login(given)

    expected = {
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": '21'
    }   
    assert user_logged == expected

def test_path_profile(create_and_delete_db):

    #Sign-up user
    given = {   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": '21'
    }
    signup(given)
    
    #Patch profile
    given = {   
        "age": '21'
    }
    patched_profile = path_profile(given)

    expected = {
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": '21'
    }
    assert patched_profile == expected

def test_delete_profile(create_and_delete_db):

    #Sign-up user
    given = {   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": '21'
    }
    signup(given)

    #Try delete user
    given = ""
    try_delete = delete_profile(given)

    expected = "STATUS 204 - NO CONTENT"
    assert expected == try_delete

def test_get_profile(create_and_delete_db):
    #Sign-up two same users
    given1 = {   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "password": "imgoingtobeahokage123",
        "age": '21'
    }
    given2 = {   
        "name": "Tobirama Shimatzu",
        "email": "tobirama@konoha.com",
        "password": "thisactivitieissupercool",
        "age": '26'
    }

    signup(given1)
    signup(given2)

    #Get users in DB
    find_users = get_users()

    expected = [{   
        "name": "Naruto Uzumaki",
        "email": "naruto@konoha.com",
        "age": '21'
    },
    {   
        "name": "Tobirama Shimatzu",
        "email": "tobirama@konoha.com",
        "age": '26'
    }
    ]

    assert find_users == expected