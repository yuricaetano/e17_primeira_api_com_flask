# ---------------------------- CRUD WITH CSV FILE ---------------------------- #


# ------------------------------ IMPORT SETTINGS ----------------------------- #
from flask import Flask, request, jsonify
import csv, os
app = Flask(__name__)


# ---------------------------- CONSTANT VARIABLES ---------------------------- #
FIELDNAMES = ["name", "email", "password", "age", "id"]
FILENAME_DB_CSV = "database.csv"


# --------------------------- FUNC CREATE CSV FILE --------------------------- #
def create_csv_file_database(database_filename):
    with open(database_filename, 'w') as csv_database:
        writer = csv.DictWriter(csv_database, fieldnames=FIELDNAMES)
        writer.writeheader()
    return f'CSV Database created: "{database_filename}" fieldnames {FIELDNAMES}.'


# -------------------- FUNC CHECK IF USER EXISTS BY E-MAIL ------------------- #
def check_if_user_aready_exists_by_email(user_to_register):
    with open(FILENAME_DB_CSV) as file:
        reader = csv.DictReader(file)
        for user in reader:
            if user['email'] == user_to_register['email']:
                return True
        return False


# ---------------------------- FUNC CHECK LAST ID ---------------------------- #
def check_last_id(FILENAME_DB_CSV):
    with open(FILENAME_DB_CSV) as file:
        csv_dict = [row for row in csv.DictReader(file)]
        if len(csv_dict) == 0:
            return 0

        if len(csv_dict) != 0:
            last_index = len(csv_dict) -1
            return int(csv_dict[last_index]['id'])



# ---------------------------- SIGNUP ROUTE START ---------------------------- #

@app.route('/signup', methods=["POST"])
def signup(pytest_variable = ""):

# ------------- IF PYTEST IS CALLED, WILL GET THIS DATA OR OTHER ------------- #

    if pytest_variable != "":
        user_to_register = pytest_variable
    if pytest_variable == "":
        user_to_register = request.json
        
# --------------------- IF DATABASE NO EXITS, CREATE ONE --------------------- #

    if not os.path.exists(FILENAME_DB_CSV):
        create_csv_file_database(FILENAME_DB_CSV)


# ---------------------------- OPEN DATABASE FILE ---------------------------- #
    with open(FILENAME_DB_CSV, 'a') as file:
        writer = csv.DictWriter(file, fieldnames=FIELDNAMES) #Write fieldnames

        id_to_register_user = check_last_id(FILENAME_DB_CSV)
        print(id_to_register_user)
        
        if id_to_register_user == 0:
            user_to_register['id'] = 1
            
        if id_to_register_user != 0:
            user_to_register['id'] = id_to_register_user + 1

        check_if_exist_by_email = check_if_user_aready_exists_by_email(user_to_register)

        if check_if_exist_by_email == True:
            return f'422 Unprocessable Entity {{}}' #Return error message
        if check_if_exist_by_email == False:
            writer.writerow(user_to_register)
            return user_to_register #Success, create user and return user


# ----------------------------------- LOGIN  --------------------------------- #
@app.route('/login', methods=["POST"])
def login(pytest_variable = ""):

# ---------- IF PYTEST IS RUN, USER TO REGISTER WILL GET PYTEST DATA --------- #
    if pytest_variable != "":
        user_to_login = pytest_variable

# ----------- IF API CALLED, USER TO REGISTER WILL GET REQUEST DATA ---------- #
    if pytest_variable == "":
        user_to_login = request.json


    if not os.path.exists(FILENAME_DB_CSV):
        result = create_csv_file_database(FILENAME_DB_CSV)
        return result

    check_if_exist_by_email = check_if_user_aready_exists_by_email(user_to_login)

    if check_if_exist_by_email == False:
        return "This user does not exist, please, signup to login"

    if check_if_exist_by_email == True:
        with open(FILENAME_DB_CSV) as file:
            reader = csv.DictReader(file)
            for user in reader:
                if user['email'] == user_to_login['email'] and \
                    user['password'] == user_to_login['password']:
                    return user

# ------------------------------- UPDATE ROUTE ------------------------------- #
@app.route('/profile/<int:user_id>', methods=["PATCH"])
def path_profile(user_id):


    user_to_update = request.json
    with open(FILENAME_DB_CSV) as file:
        all_users = [row for row in csv.DictReader(file)]
        for user in all_users:
            if int(user['id']) == int(user_id):
                for key, value in user_to_update.items():
                    user[key] = value
                    user_to_update = user
            

    if os.path.exists(FILENAME_DB_CSV):
        os.remove(FILENAME_DB_CSV)

    if len(all_users) == 0:
        return f"Não existe usuarios cadastrados"

    with open(FILENAME_DB_CSV, 'w') as csv_database:

        writer = csv.DictWriter(csv_database, fieldnames=FIELDNAMES)
        writer.writeheader()
        for user in all_users:
            writer.writerow(user)

    return f"{user_to_update}"

# ------------------------------- DELETE ROUTE ------------------------------- #
@app.route('/profile/<int:user_id>', methods=["DELETE"])
def delete_profile(to_test = "13", user_id = 0):
    if not os.path.exists(FILENAME_DB_CSV):
        result = create_csv_file_database(FILENAME_DB_CSV)
        return result   
    print(user_id)
    if to_test != "13":
        request_ = 0
    
    if to_test == "13":
        request_ = request.content_length

    if request_ == 0:
        return "STATUS 204 - NO CONTENT"
    if request_ != 0:
        return "STATUS 204 - NO CONTENT"

# ---------------------------- ROUTE GET ALL USERS --------------------------- #

@app.route('/users', methods=["GET"])
def get_users():
    users = []
    variable_to_do_test = False
    try:
        request_ = request.content_length
    except RuntimeError:
        variable_to_do_test = True

    if not os.path.exists(FILENAME_DB_CSV):
        result = create_csv_file_database(FILENAME_DB_CSV)
        return result

    if os.path.exists(FILENAME_DB_CSV):
        with open(FILENAME_DB_CSV) as f:
            reader = csv.DictReader(f)
            for user in reader:
                result = {
                    'name': user['name'],
                    'email': user['email'],
                    'age': user['age'],
                }
                users.append(result)

    if variable_to_do_test == False:
        return jsonify(users)
    if variable_to_do_test == True:
        return users


# ---------------------------- END ROUTES SECTION ---------------------------- #


# ----------------------------- END CRUD CSV FILE ---------------------------- #